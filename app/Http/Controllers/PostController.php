<?php

namespace App\Http\Controllers;

use App\Repositories\PostRepository;
use App\Http\Requests\PostRequest;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    protected $postRepository;
    protected $nbrPerPage = 4;

    public function __construct(PostRepository $postRepository)
    {
        $this->middleware('auth')->except('index');
        $this->middleware('admin')->only('destroy');
        $this->postRepository = $postRepository;
    }

    public function index()
    {
        $posts = $this->postRepository->getPaginate($this->nbrPerPage);
        $links = $posts->render();

        return view('posts.liste', compact('posts', 'links'));
    }

    public function create()
    {
        return view('posts.add');
    }

    public function store(PostRequest $request)
    {
        $inputs = array_merge($request->all(), ['user_id' => Auth::id()]);
        $this->postRepository->store($inputs);

        return redirect()->route('post.index');
    }

    public function edit($id)
    {
        $post = $this->postRepository->find($id);
    
        return view('posts.edit', compact('post'));
    }

    public function destroy($id)
    {
        $this->postRepository->destroy($id);

        return redirect()->back();
    }
    public function indexTag($tag)
{
    $postRepository = app(PostRepository::class);
    $posts = $postRepository->getWithUserAndTagsForTagPaginate($tag, $this->nbrPerPage);
    $links = $posts->render();

    return view('posts.liste', compact('posts', 'links'))
        ->with('info', 'Résultats pour la recherche du mot-clé : ' . $tag);
}
}

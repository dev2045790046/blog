<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class Admin
{
    public function handle(Request $request, Closure $next)
    {
        if (!auth()->check()) {
            return Redirect::route('login');
        }

        if (!auth()->user()->isAdmin()) {
            return Redirect::route('post.index');
        }

        return $next($request);
    }
}

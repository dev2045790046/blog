<?php

namespace App\Models;
use App\Models\Tag;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['titre', 'contenu', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function tags()
{
         return $this->belongsToMany(Tag::class);
}

}

<?php

namespace App\Repositories;

use App\Models\Post;

class PostRepository 
{
    protected Post $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    // Méthode pour récupérer les publications avec leurs utilisateurs associés paginées
    public function getPaginate(int $n)
    {
        return $this->post->with('user')
            ->orderBy('created_at', 'desc')
            ->paginate($n);
    }

    // Méthode pour stocker une nouvelle publication
    public function store(array $inputs): \App\Models\Post
    {
        return $this->post->create($inputs);
    }

    // Méthode pour supprimer une publication par son ID
    public function destroy(int $id): void
    {
        $post = $this->post->find($id);
        if ($post) {
            $post->delete();
        }
    }

    // Méthode pour trouver une publication par son ID
    public function find(int $id): ?\App\Models\Post
    {
        return $this->post->find($id);
    }

    // Méthode pour récupérer les publications avec leurs tags associés paginées pour un tag spécifique
    public function getWithUserAndTagsForTagPaginate(string $tag, int $n)
    {
        return $this->post->with('user', 'tags')
            ->whereHas('tags', function($q) use ($tag) {
                $q->where('tags.tag_url', $tag);
            })
            ->orderBy('created_at', 'desc')
            ->paginate($n);
    }
}
?>

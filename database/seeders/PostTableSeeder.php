<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB; // Importer le façade DB
use Carbon\Carbon;

class PostTableSeeder extends Seeder
{
    private function randDate()
    {
        return Carbon::createFromDate(null, rand(1, 12), rand(1, 28));
    }

    public function run()
    {
        DB::table('posts')->delete();
        
        // Assurez-vous que la table 'users' est déjà remplie avec des données
        // avant d'exécuter ce seeder, sinon les clés étrangères ne seront pas résolues
        // correctement.
        
        for ($i = 0; $i < 100; $i++) {
            $date = $this->randDate();
            DB::table('posts')->insert([
                'titre' => 'Titre' . $i,
                'contenu' => 'Contenu' . $i . 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui',
                'user_id' => rand(1, 10), // Assurez-vous que la table 'users' est déjà remplie avec des données
                'created_at' => $date,
                'updated_at' => $date
            ]);
        }
    }
}


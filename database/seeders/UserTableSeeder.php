<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        // Supprimer les posts associés à chaque utilisateur
        DB::table('posts')->truncate();

        // Supprimer les utilisateurs
        DB::table('users')->truncate();

        // Ajouter ici votre logique pour semer (seed) les utilisateurs
        for ($i = 0; $i < 10; ++$i) {
            DB::table('users')->insert([
                'name' => 'Nom' . $i,
                'email' => 'email' . $i . '@blop.fr',
                'password' => bcrypt('password' . $i),
                'admin' => rand(0, 1)
            ]);
        }
    }
}

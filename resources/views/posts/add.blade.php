{{-- <!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ajout d'un article</title>
    <!-- Inclure les fichiers CSS de Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
    <style>
        .has-error input,
        .has-error textarea {
            border-color: #a94442;
        }

        .has-error small {
            color: #a94442;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-offset-3 col-sm-6">
                <div class="panel panel-info">
                    <div class="panel-heading">Ajout d'un article</div>
                    <div class="panel-body">
                        <form action="{{ route('post.store') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <input type="text" name="titre" class="form-control" placeholder="Titre">
                                @error('titre')
                                    <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group">
                                <textarea name="contenu" class="form-control" placeholder="Contenu"></textarea>
                                @error('contenu')
                                    <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-info pull-right">Envoyer !</button>
                        </form>
                    </div>
                </div>
                <a href="javascript:history.back()" class="btn btn-primary">
                    <span class="glyphicon glyphicon-circle-arrow-left"></span> Retour
                </a>
            </div>
        </div>
    </div>
    <!-- Inclure les fichiers JavaScript de Bootstrap (si nécessaire) -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</body>
</html> --}}
@extends('template')

@section('contenu')
<br>
<div class="col-sm-offset-3 col-sm-6">
    <div class="panel panel-info">
        <div class="panel-heading">Ajout d'un article</div>
        <div class="panel-body">
            <form method="POST" action="{{ route('post.store') }}">
                @csrf
                <div class="form-group {{ $errors->has('titre') ? 'has-error' : '' }}">
                    <input type="text" name="titre" class="form-control" placeholder="Titre">
                    @error('titre')
                    <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group {{ $errors->has('contenu') ? 'has-error' : '' }}">
                    <textarea name="contenu" class="form-control" placeholder="Contenu"></textarea>
                    @error('contenu')
                    <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group {{ $errors->has('tags') ? 'has-error' : '' }}">
                    <input type="text" name="tags" class="form-control" placeholder="Entrez les mots-clés séparés par des virgules">
                    @error('tags')
                    <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <button type="submit" class="btn btn-info pull-right">Envoyer !</button>
            </form>
        </div>
    </div>
    <a href="javascript:history.back()" class="btn btn-primary">
        <span class="glyphicon glyphicon-circle-arrow-left"></span> Retour
    </a>
</div>
@endsection

{{-- @extends('template')
@section('header')
<div class="container">
  <div class="row">
      <div class="col-md-12">
          <div class="header">
              @if(Auth::check())
                  <div class="btn-group pull-right">
                      <a href="{{ route('post.create') }}" class="btn btn-info">Créer un article</a>
                      <form action="{{ route('logout') }}" method="POST">
                          @csrf
                          <button type="submit" class="btn btn-warning">Déconnexion</button>
                      </form>
                  </div>
              @else
                  <a href="{{ route('login') }}" class="btn btn-info pull-right">Se connecter</a>
              @endif
          </div>
      </div>
  </div>
  @if(isset($info))
      <div class="row alert alert-info">{{$info}}</div>
  @endif
  {!!$links!!}
  @foreach($posts as $post)
      <article class="row bg-primary">
          <div class="col-md-12">
              <header>
                  <h1>{{$post->titre}}</h1>
              </header>
              <hr>
              <section>
                  <p>{{$post->contenu}}</p>
                  @if(Auth::check() && Auth::user()->admin)
                      <form action="{{ route('post.destroy', $post->id) }}" method="POST">
                          @csrf
                          @method('DELETE')
                          <button type="submit" class="btn btn-danger">Supprimer cet article</button>
                      </form>
                  @endif
                  <em class="pull-right">
                      <span class="glyphicon glyphicon-pencil"></span> 
                      {{$post->user->name}} le {{ $post->created_at->format('d-m-Y') }}
                  </em>
              </section>
          </div>
      </article>
  @endforeach
  {!!$links!!}
</div>
<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"> --}}



















{{-- @extends('template')

@section('header')
@if(Auth::check())
<div class="btn-group pull-right">
    <a href="{{ route('post.create') }}" class="btn btn-info">Créer un article</a>
    <a href="{{ route('logout') }}" class="btn btn-warning">Déconnexion</a>
</div>
@else
    <a href="{{ route('login') }}" class="btn btn-info pull-right">Se connecter</a>
@endif
@endsection

@section('contenu')
@if(isset($info))
<div class="row alert alert-info">{{$info}}</div>
@endif
{!! $links !!}
@foreach($posts as $post)
<article class="row bg-primary">
    <div class="col-md-12">
        <header>
            <h1>{{$post->titre}}</h1>
        </header>
        <hr>
        <section>
            <p>{{$post->contenu}}</p>
            @if(Auth::check() && Auth::user()->admin)
            <form method="POST" action="{{ route('post.destroy', $post->id) }}">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Supprimer cet article</button>
            </form>
            @endif
            <em class="pull-right">
                <span class="glyphicon glyphicon-pencil"></span>
                {{$post->user->name}} le {{ $post->created_at->format('d-m-Y') }}
            </em>
        </section>
    </div>
</article>
@endforeach
@endsection --}}




@extends('template')

@section('header')
@if(Auth::check())
<div class="btn-group pull-right">
    <a href="{{ route('post.create') }}" class="btn btn-info">Créer un article</a>
    <a href="{{ url('logout') }}" class="btn btn-warning">Déconnexion</a>
</div>
@else
<a href="{{ url('login') }}" class="btn btn-info pull-right">Se connecter</a>
@endif
@endsection

@section('contenu')
@if(isset($info))
<div class="row alert alert-info">{{ $info }}</div>
@endif

{{ $links }}

@foreach($posts as $post)
<article class="row bg-primary">
    <div class="col-md-12">
        <header>
            <h1>{{ $post->titre }}
                <div class="pull-right">
                    @foreach($post->tags as $tag)
                    <a href="{{ url('post/tag/' . $tag->tag_url) }}" class="btn btn-xs btn-info">{{ $tag->tag }}</a>
                    @endforeach
                </div>
            </h1>
        </header>
        <hr>
        <section>
            <p>{{ $post->contenu }}</p>
            @if(Auth::check() && Auth::user()->admin)
            {!! Form::open(['method'=>'DELETE', 'route'=>['post.destroy', $post->id]]) !!}
            {!! Form::submit('Supprimer cet article', ['class'=>'btn article']) !!}
            {!! Form::close() !!}
            @endif
            <em class="pull-right">
                <span class="glyphicon glyphicon-pencil"></span>{{ $post->user->name }} le {{ $post->created_at->format('d-m-Y') }}
            </em>
        </section>
    </div>
</article>
<br>
@endforeach

{{ $links }}
@endsection

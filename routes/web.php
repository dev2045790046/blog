<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Définition des routes pour les articles
Route::resource('post', PostController::class)->except(['show', 'edit', 'update']);

// Route pour l'affichage du formulaire de connexion
Route::get('/login', [LoginController::class, 'showLoginForm'])->name('login');

// Route pour la page d'accueil après la connexion
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Route pour la déconnexion (accepte à la fois les requêtes GET et POST)
Route::match(['get', 'post'], '/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');

// Route pour la soumission du formulaire de connexion
Route::post('/login', [LoginController::class, 'login']);

Route::get('post/tag/{tag}', [PostController::class, 'indexTag']);